/* A program to demonstrate Serialization for various different dependencies */

import java.io.*; 
//if base class implements Serializable interface then child class need not implement Serializable interface
class Demo extends abc 
{  
 public int a; 
 public String b; 
 def D; 
 // Default constructor 
 public Demo(String place, int zipcode, int a, String b,int id,String name) 
 { 
    super(place,zipcode);
     D = new def( id, name);
     this.a = a; 
     this.b = b; 
 } 
} 

class Test 
{ 
 public static void main(String[] args) 
 {    
     Demo object = new Demo("hyderabad",518501,1, "geeksforgeeks",12,"pradeep"); 
     String filename = "file.ser"; 
       
     // Serialization  
     try
     {    
         //Saving of object in a file 
         FileOutputStream file = new FileOutputStream(filename); 
         ObjectOutputStream out = new ObjectOutputStream(file); 
           
         // Method for serialization of object 
         out.writeObject(object); 
           
         out.close(); 
         file.close(); 
           
         System.out.println("Object has been serialized"); 

     } 
       
     catch(IOException ex) 
     { 
         System.out.println("IOException is caught"+ex); 
     } 


     Demo object1 = null; 
     def object2 = null;

     // Deserialization 
     try
     {    
         // Reading the object from a file 
         FileInputStream file = new FileInputStream(filename); 
         ObjectInputStream in = new ObjectInputStream(file); 
           
         // Method for deserialization of object 
         object1 = (Demo)in.readObject(); 
	//object2 = (def)in.readObject();

           
         in.close(); 
         file.close(); 
           
         System.out.println("Object has been deserialized "); 
         System.out.println("a = " + object1.a); 
         System.out.println("b = " + object1.b); 
	System.out.println("b = " + object1.place); 
	System.out.println("b = " + object1.zipcode); 
	System.out.println("b = " + object1.D.name); 
	System.out.println("b = " + object1.D.id); 
     } 
       
     catch(IOException ex) 
     { 
         System.out.println("IOException is caught"+ex); 
     } 
       
     catch(ClassNotFoundException ex) 
     { 
         System.out.println("ClassNotFoundException is caught"); 
     } 

 } 
} 